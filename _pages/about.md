---
title: About
layout: text
permalink: /about
---
**Sadi Muktadir** is a writer from Toronto.

His debut novel, *Land of No Regrets*, was published by HarperCollins Canada and Hanover Square Press on May 21st, 2024. His short stories have appeared in Joyland Magazine, the Humber Literary Review, Blank Spaces, The New Quarterly and other places. He is a two-time finalist for the Thomas Morton Memorial Prize in Literary Excellence and twice shortlisted for the Malahat Open Season Awards for best short fiction.

He works as an Editor, and continues to read and write. Find him online at **@sadi_muktadir** on both [Twitter](https://twitter.com/sadi_muktadir?lang=en) and [Instagram](https://www.instagram.com/sadi_muktadir).
