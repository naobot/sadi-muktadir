---
layout: works
title: Other Writing
permalink: /writing
---

## Fiction

Quadruple Bypass - *Joyland Magazine*

Tongueless and Laceless - *The New Quarterly*

Shutki and Tandoor - *Ricepaper Magazine*

Two Dead Dreams - *The Trinity Review*

The Other Ramadan Story - *Blank Spaces Magazine*

## Non-fiction

COVID-19 Vaccine hesitancy - CBC