module.exports = {
  content: [
    './_drafts/**/*.html',
    './_includes/**/*.html',
    './_layouts/**/*.html',
    './_posts/*.md',
    './*.md',
    './*.html',
  ],
  theme: {
    extend: {
      colors: {
        'accent': '#E4553F',
      },
    },
    theme: {
      extend: {}
    },
  },
  plugins: []
}
